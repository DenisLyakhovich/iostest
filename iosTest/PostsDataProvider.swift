//
//  PostsDataProvider.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 19.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import UIKit

protocol PostDataProviderDelegate: class {
    func postDataLoaded()
    func postDataHasError(error: String)
}

class PostsDataProvider: NSObject {
    
    var  posts: [PostModel] = []
    weak var delegate: PostDataProviderDelegate?

    init(d: PostDataProviderDelegate) {
        self.delegate = d
    }
    
    func refresh() {
        posts = []
        loadPosts()
    }
    
    func loadPosts() {
        TestProjectApiService.sharedInstance.blogApiService.posts(limit: 100, skip: posts.count, complection: { (postsReponse, error) in
            
            if let error = error {
                self.delegate?.postDataHasError(error: error)
                return
            }else if let postArray:[PostModel] = postsReponse{
                self.posts.append(contentsOf: postArray)
                self.delegate?.postDataLoaded()
                print("Load Posts")
                return
            }
           self.delegate?.postDataLoaded()
        })
    }
}
