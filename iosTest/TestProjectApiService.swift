//
//  TestProjectApiService.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 19.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import UIKit

class TestProjectApiService: NSObject {
    
    static let sharedInstance: TestProjectApiService = {
        TestProjectApiService()
    }()
    
    private(set) var blogApiService: BlogsApiServices
    private(set) var articleApiService: ArticleApiServices
    
    private override init() {
    
      self.blogApiService = BlogsApiServices()
      self.articleApiService = ArticleApiServices()
    }
}
