//
//  ArticleCommentTableViewCell.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 08.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import UIKit

class ArticleCommentTableViewCell: UITableViewCell {
    
    static let nibName = "ArticleCommentTableViewCell"
    
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImageView.layer.cornerRadius = userImageView.frame.height / 2.0
        userImageView.layer.masksToBounds = true
        // Initialization code
    }
    
    func  castomize(comment: CommetsModel) {
        commentLabel.text = comment.body
        nameLabel.text = comment.name
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }    
}
