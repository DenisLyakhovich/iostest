//
//  ArticleDataProvider.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 27.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import UIKit

protocol ArticleDataProviderDelegate: class {
    func articleDataLoaded()
    func articleDataHasError(error: String)
    
}

class ArticleDataProvider: NSObject {
    
    var  comments: [CommetsModel] = []
    
    weak var delegate: ArticleDataProviderDelegate?
    
    init(d: ArticleDataProviderDelegate) {
        self.delegate = d
    }
    
    func loadComments(index: PostModel) {
        TestProjectApiService.sharedInstance.articleApiService.articles(limit: 500, skip: comments.count, complection: { (commentsReponse, error) in
            
            if let error = error {
                
                self.delegate?.articleDataHasError(error: error)
                return
            }else if let commentsArray:[CommetsModel] = commentsReponse {
                
                for element in commentsArray {
                    if element.postId == index.id {
                        self.comments.append(element)
                    }
                }
                self.delegate?.articleDataLoaded()
                print("Load comments")
                print("PostID \(String(describing: index.id))")
                return
                
            }
            self.delegate?.articleDataLoaded()
        })
    }

    
    
    
    
    
    
    
    
    
    
    
}
