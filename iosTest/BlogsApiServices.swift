//
//  BlogsApiServices.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 19.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyJSON

typealias PostComplection = (_ posts: [PostModel]?, _ error: String?) -> Void

class BlogsApiServices: BaseApiService {
    
    func  posts (limit: Int, skip: Int, complection: @escaping PostComplection)  {
        let url = host + "/posts"
        
        var params:[String:AnyObject] = [:]
        params["limit"] = limit as AnyObject
        params["skip"] = skip as AnyObject
        
        self.sendRequestWithJSONResponse(
            requestType: .get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (responseData, error) in
                
                if error != nil {
                    complection(nil, error?.localizedDescription)
                    return
                } else if let posts = responseData!.arrayObject {
                    let postModel = Mapper<PostModel>().mapArray(JSONObject: posts)
                    
                    complection(postModel, nil)
                    return
                }
                complection(nil, "Ошибка при загрузке статей")
        }
    }    
}
    

    
    
    
    
    
    


