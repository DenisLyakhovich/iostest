//
//  ArticleDescriptViewController.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 08.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import UIKit

class ArticleDescriptViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    
    @IBOutlet weak var tableViewDescript: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewDescript.register(UINib(nibName:"ArticleDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "ArticleDescriptionTableViewCell")
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - TableView
    
    func  tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var tableViewCell: ArticleDescriptionTableViewCell!
        tableViewCell = tableViewDescript.dequeueReusableCell(withIdentifier: "ArticleDescriptionTableViewCell") as!
        ArticleDescriptionTableViewCell
        
        return tableViewCell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
   
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
