//
//  ArticleViewController.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 07.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import UIKit
import Alamofire

class ArticleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ArticleDataProviderDelegate {
    
    @IBOutlet weak var tableView: UITableView!

    var dataProviderComments: ArticleDataProvider?
    var blog: PostModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: ArticleDescriptionTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ArticleDescriptionTableViewCell.nibName)
        tableView.register(UINib(nibName:ArticleTitleTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ArticleTitleTableViewCell.nibName)
        tableView.register(UINib(nibName:ArticleCommentTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ArticleCommentTableViewCell.nibName)
       
        dataProviderComments = ArticleDataProvider(d: self)
        dataProviderComments?.loadComments(index: blog!)
        
        //self.navigationItem.hidesBackButton = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        if dataProviderComments == nil {
            return 2
        }
        return 2
    }
    
    func  tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        } else {
            return dataProviderComments!.comments.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: ArticleTitleTableViewCell.nibName) as! ArticleTitleTableViewCell
                cell.castomize(post:blog!)
                return cell
                
            } else {
                let  cell = tableView.dequeueReusableCell(withIdentifier: ArticleDescriptionTableViewCell.nibName)as!
                ArticleDescriptionTableViewCell
                cell.castomize(post: blog!)
                return cell
            }
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ArticleCommentTableViewCell.nibName) as!
            ArticleCommentTableViewCell
            cell.castomize(comment:  (dataProviderComments?.comments[indexPath.row])!)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if indexPath.row == 0 {
                return 120
            } else {
                self.tableView.estimatedRowHeight = 2
                self.tableView.rowHeight = UITableViewAutomaticDimension
                return UITableViewAutomaticDimension
            }
        } else {
           return 90
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: - ArticleDataProvider
    func articleDataLoaded() {
        print("Reload comments")
        tableView.reloadData()
    }
    
    func articleDataHasError(error: String) {
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
