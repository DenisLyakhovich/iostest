//
//  ArticleDescriptionViewController.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 07.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import UIKit

class ArticleDescriptionViewController: ArticleViewController {


    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewDescription.register(UINib(nibName:"ArticleDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "ArticleDescriptionTableViewCell")
        tableView.register(UINib(nibName:"ArticleTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "ArticleTitleTableViewCell")
        tableViewComment.register(UINib(nibName:"ArticleCommentTableViewCell", bundle: nil), forCellReuseIdentifier: "ArticleCommentTableViewCell")
         // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func  tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var tableViewCell: UITableViewCell!
        
        if indexPath.row == 1 {
        //var tableViewCell: ArticleDescriptionTableViewCell!
        tableViewCell = tableViewDescription.dequeueReusableCell(withIdentifier: "ArticleDescriptionTableViewCell") as! ArticleDescriptionTableViewCell
        } else if indexPath.row == 2 {
          //  var tableViewCell: ArticleTitleTableViewCell!
            tableViewCell = tableViewComment.dequeueReusableCell(withIdentifier: "ArticleCommentTableViewCell") as!
            ArticleCommentTableViewCell
        } else if indexPath.row == 0 {
            //  var tableViewCell: ArticleTitleTableViewCell!
            tableViewCell = tableView.dequeueReusableCell(withIdentifier: "ArticleTitleTableViewCell") as!
            ArticleTitleTableViewCell
        }
        
        return tableViewCell!
    }

   override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
