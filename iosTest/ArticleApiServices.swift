//
//  ArticleApiServices.swift
//  iosTest
//
//  Created by Denis Lyakhovich on 27.12.17.
//  Copyright © 2017 Denis Lyakhovich. All rights reserved.
//

import Alamofire
import SwiftyJSON
import ObjectMapper

typealias ArticleComplection = (_ articles: [CommetsModel]?, _ error: String?) -> Void

class ArticleApiServices: BaseApiService {
    
    func  articles (limit: Int, skip: Int, complection: @escaping ArticleComplection)  {
        let url = host + "/comments"
        
        var params:[String:AnyObject] = [:]
        params["limit"] = limit as AnyObject
        params["skip"] = skip as AnyObject
        
        self.sendRequestWithJSONResponse(
            requestType: .get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (responseData, error) in
                
                if error != nil {
                    complection(nil, error?.localizedDescription)
                    return
                } else if let articles = responseData!.arrayObject {
                    let articlesModel = Mapper<CommetsModel>().mapArray(JSONObject: articles)
                    
                    complection(articlesModel, nil)
                    return
                }
                complection(nil, "Ошибка при загрузке comments")
        }
    }
}
